import { JOB_QUEUE, WORKER_QUEUE, SHORT_DELAY,
  MEDIUM_DELAY, LONG_DELAY } from "./constants.js"

/** @param {NS} ns */
export async function main(ns) {
  ns.disableLog("sleep")
  ns.disableLog("getServerMaxRam")
  ns.disableLog("getServerUsedRam")
  //const messages = ns.getPortHandle(MESSAGE_QUEUE)
  //const jobs = ns.getPortHandle(JOB_QUEUE)
  //const workers = ns.getPortHandle(WORKER_QUEUE)

  while (true) {
    await ns.sleep(MEDIUM_DELAY)

    let job_data = "" + ns.readPort(JOB_QUEUE)
    if (job_data == "NULL PORT DATA") {
      ns.print("No jobs on the queue.  Sleeping")
      await ns.sleep(LONG_DELAY))
      continue
    }

    ns.print("Received job data: " + job_data)
    const arr = job_data.split(",")

    if (arr.length !== 3) {
      ns.print("  Invalid job data on the queue: " + job_data)
      continue
    }

    const target_server = arr[0]
    const job = arr[1]
    const script_mem = job == "hack" ? 1.7 : 1.75
    const job_script = "lock_" + job + ".js"
    let requested_threads = 0 + Math.floor(arr[2])

    if (!ns.serverExists(target_server)) {
      ns.print("  Invalid server name on job queue: --" + target_server + "--")
      continue
    }

    ns.print("  Processing job data - target_server: " + target_server + ", job: " + job +
      ", requested_threads: " + requested_threads)

    while (requested_threads > 0) {
      await ns.sleep(SHORT_DELAY)
      ns.print("  Requested threads: " + requested_threads)

      const host_server = ns.readPort(WORKER_QUEUE)
      if (host_server == "NULL PORT DATA") {
        ns.print("  Worker queue empty")
        await ns.sleep(LONG_DELAY)
        continue
      }

      if (!ns.serverExists(host_server)) {
        ns.print("  Invalid server name on worker queue: --" + host_server + "--")
        continue
      }

      if (!ns.fileExists(job_script, host_server)) {
        ns.print("  File " + job_script + " not found on server " + host_server)
        continue
      }

      const worker_threads = Math.floor((ns.getServerMaxRam(host_server) -
        ns.getServerUsedRam(host_server)) / script_mem)

      if (worker_threads <= 0) {
        ns.print("No worker threads found")
        continue
      }

      let actual_threads = 0

      ns.print("  worker_threads: " + worker_threads
        + ", requested threads: " + requested_threads)

      if (worker_threads > requested_threads) {
        actual_threads = requested_threads
        requested_threads = 0
        ns.tryWritePort(WORKER_QUEUE, host_server)
      } else {
        actual_threads = worker_threads
        requested_threads = requested_threads - actual_threads
      }

      if (actual_threads > 0) {
        const lock_file = host_server + "." + job + "." + target_server + ".lock.txt"
        ns.print("  Calling exec: " + job_script + ", " + host_server + ", " +
          actual_threads + ", " + host_server + ", " + target_server + ", " +
          actual_threads)

        const pid = ns.exec(job_script, host_server, actual_threads, host_server,
          target_server, actual_threads)
        if (pid > 0) {
          ns.write(lock_file, "1", "w")
        }
      }
    }
  }
}