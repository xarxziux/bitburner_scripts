import { WORKER_QUEUE } from "./constants.js"
import { get_core_servers, sort_by_largest_value } from "./utils.js"

/** @param {NS} ns */
export async function main(ns) {
  ns.disableLog("sleep")
  ns.disableLog("getServerMaxRam")
  ns.disableLog("getServerUsedRam")

  //const queue = ns.getPortHandle(WORKER_QUEUE)
  const servers = get_core_servers()

  while (true) {
    const details = []

    servers.forEach((server) => {
      if (ns.hasRootAccess(server)) {
        const free_ram = ns.getServerMaxRam(server) - ns.getServerUsedRam(server)

        if (free_ram >= 2) {
          details.push({
            server: server,
            value: free_ram
          })
        }
      }
    })

    if (details.length == 0) {
      ns.print("No workers available")
      continue
    }

    details.sort(sort_by_largest_value)

    for (const detail of details) {
      const ok = ns.tryWritePort(WORKER_QUEUE, detail.server)
      if (!ok) {
        ns.print("Worker queue full")
        break
      }

      await ns.sleep(200)
    }

    await ns.sleep(10000)
  }
}
