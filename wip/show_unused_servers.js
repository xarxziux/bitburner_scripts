import { get_servers } from "./get_servers.js"

/** @param {NS} ns */
export async function main(ns) {
  get_servers().forEach((server) => {
    if (!ns.hasRootAccess(server)) {
      return
    }

    if (ns.getServerMaxRam(server) == 0) {
      return
    }

    if (ns.getServerUsedRam(server) > 0) {
      return
    }

    ns.tprint(server + ": " + ns.getServerMaxRam(server))
  })
}