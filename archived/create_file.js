/** @param {NS} ns */
export async function main(ns) {
  ns.write("target.txt", "n00dles", "w")
  ns.scp("target.txt", "n00dles")


  if (ns.fileExists("target.txt", "n00dles")) {
    ns.tprint("Success")
  } else {
    ns.tprint("Failure")
  }
}