/** @param {NS} ns */
export async function main(ns) {
  const server = ns.args[0]
  const threads = ns.args[1]
  const max_money = ns.getServerMaxMoney(server)

  if (ns.getServerMoneyAvailable < max_money) {
    await ns.grow(server, { threads: threads })
  }
}