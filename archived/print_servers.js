import {get_servers} from "./get_servers.js"

/** @param {NS} ns */
export async function main(ns) {
  const servers = get_servers()

  servers.forEach(server => {
    ns.tprint(server + ": " + Math.floor(ns.getServerMoneyAvailable(server) / 100000))
  })
}