/** @param {NS} ns */

export async function main(ns) {
  const h = ns.getHostname();

  while (true) {
    if (ns.getServerSecurityLevel(h) > 5) {
      await ns.weaken(h);
    } else {
      await ns.grow(h);
      await ns.hack(h);
    }
  }
}