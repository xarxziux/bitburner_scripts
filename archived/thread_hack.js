/** @param {NS} ns */
export async function main(ns) {
  const server = ns.args[0]
  const threads = ns.args[1]
  const min_hack = ns.getServerRequiredHackingLevel(server)

  if (ns.getHackingLevel() >= min_hack) {
    await ns.hack(server, { threads: threads })
  }
}