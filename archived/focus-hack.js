/** @param {NS} ns */
export async function main(ns) {
  const host_server = ns.getHostname()
  const target_server = ns.args[0]

  while (true) {
    await weaken(ns, host_server, target_server)
    await grow(ns, host_server, target_server)
    await weaken(ns, host_server, target_server)
    await hack(ns, host_server, target_server)
  }
}

/** 
 * @param {NS} ns 
 * @param (string) host_server
 * @param (string) target_server
 * @param (number) threads
*/
async function weaken(ns, host_server, target_server, requested_threads) {
  const curr_sec = ns.getServerSecurityLevel(target_server)
  const min_sec = ns.getServerMinSecurityLevel(target_server)
  let required_threads = Math.floor((curr_sec - min_sec) * 20)

  while (required_threads > 0) {
    let available_threads = Math.floor((ns.getServerMaxRam(host_server) -
      ns.getServerUsedRam(host_server)) / 1.75)
      
    while (available_threads <= 0) {
      await ns.sleep(20000)
      available_threads = Math.floor((ns.getServerMaxRam(host_server) -
        ns.getServerUsedRam(host_server)) / 1.75)

    }

    const actual_threads = Math.min(requested_threads, available_threads)
    required_threads - required_threads - actual_threads
    const pid = ns.run("simple_weaken.js", actual_threads, target_server, actual_threads)

    if (pid == 0) {
      ns.print("Failed to start weaken script")
      return
    }

    if (ns.isRunning(pid)) {
      await ns.sleep(10000)
    }
  }
}