/** @param {NS} ns */

export async function main(ns) {
  const server = ns.args[0]
  let threads = ns.args[1]
  if (threads == 0){
    threads = 1
  }

  if (!ns.serverExists(server)) {
    ns.tprint(Server + + server + " not found")
    return
  }

  if (!ns.hasRootAccess(server)) {
    ns.tprint("No root access to " + server)
    return
  }

  const min_security = ns.getServerMinSecurityLevel(server) * 1.1
  const max_money = ns.getServerMaxMoney(server) / 1.1
  const min_hack_level = ns.getServerRequiredHackingLevel(server)

  while (true) {
    if (ns.getServerSecurityLevel(server) > min_security) {
      await ns.weaken(server, {threads: threads});
    } else if (ns.getServerMoneyAvailable < max_money) {
      await ns.grow(server, {threads: threads})
    } else if (ns.getHackingLevel() >= min_hack_level) {
      await ns.hack(server, {threads: threads})
    } else {
      await ns.sleep(600000)
    }
  }
}