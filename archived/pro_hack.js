/** @param {NS} ns */
export async function main(ns) {
  const target_server = ns.args[0] 
  const host_server = target_server + "-hack"
  const server_ram = ns.args[1]

  ns.tprint("New server cost: " + ns.getPurchasedServerCost(server_ram))
  ns.tprint("Total funds - new server cost = " + 
    (ns.getServerMoneyAvailable("home") -  ns.getPurchasedServerCost(server_ram))
  )

  const host = ns.purchaseServer(host_server, server_ram)
  ns.tprint("Host: " + host)
  ns.run("start_controller.js", 1, host_server, target_server)
}