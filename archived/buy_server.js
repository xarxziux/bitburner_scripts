import { format_number } from "utils.js"

/** @param {NS} ns */
export async function main(ns) {
  const name = ns.args[0]
  const ram = ns.args[1]

  if (name == undefined || ram == undefined){
    ns.tprint("Usage: buy_server.js <name> <ram>")
    return
  }

  const cost = ns.getPurchasedServerCost(ram)

  if (cost == 0) {
    ns.tprint("Unable to buy a server with " + ram + " ram")
    return
  }

  if (cost > ns.getServerMoneyAvailable("home")){
    ns.tprint("Not enough money available to buy server")
    return
  }

  const new_name = ns.purchaseServer(name, ram)

  if (new_name == "") {
    ns.tprint("Failed to buy new server")
    return
  }

  ns.tprint("New server created: " + new_name)
}
