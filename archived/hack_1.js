/** @param {NS} ns */
export async function main(ns) {
  const server = ns.getHostname()
  const min_hacking = s.getServerRequiredHackingLevel(server)
  const loot = 0

  if (ns.getHackingLevel() >= min_hacking) {
    loot = await ns.hack(server)
  }

  ns.tprint("Hacked " + loot + " from " + server)
}
