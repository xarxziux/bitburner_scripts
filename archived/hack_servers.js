/** @param {NS} ns */
export async function main(ns) {
  ns.args.forEach((server) => {
    ns.run("hack_server.js", 1, server)
  })
}
