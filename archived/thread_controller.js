/** @param {NS} ns */
export async function main(ns) {
  const host_server = ns.getHostname()
  const target_server = ns.args[0]
  const free_ram = ns.getServerMaxRam - ns.getServerUsedRam(host_server)
  const available_threads = Math.floor(free_ram / 2)
  const min_security = ns.getServerMinSecurityLevel(target_server) + (available_threads * 0.05)
  const max_money = ns.getServerMaxMoney(target_server)

  while (true) {
    if (ns.getServerSecurityLevel(target_server) > min_security) {
      ns.run("thread_weaken.js", { threads: available_threads }, target_server, available_threads)
    } else if (ns.getServerMoneyAvailable(target_server) < max_money) {
      ns.run("thread_grow.js", { threads: available_threads }, target_server, available_threads)
    } else {
      ns.run("thread_hack.js", { threads: available_threads }, target_server, available_threads)
    }
  }
}