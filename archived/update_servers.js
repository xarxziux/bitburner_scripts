/** @param {NS} ns */
export async function main(ns) {
  while (true) {
    for (let i = 1; i < 18; i++) {
      ns.print("Upgrading node " + i)
      if (ns.hacknet.upgradeLevel(i)) {
        ns.print("Succeeded!")
      } else {
        ns.print("Failed")
      }
      await ns.sleep(10000);
    }
  }
}