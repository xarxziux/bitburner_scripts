/** @param {NS} ns */
export async function main(ns) {
  const server = ns.getHostname()
  const max_money = ns.getServerMaxMoney(server)

  if (ns.getServerMoneyAvailable(server) < max_money) {
    await ns.grow(server)
  }

  ns.tprint("Available funds on server " + server + " is at maximum level")
}