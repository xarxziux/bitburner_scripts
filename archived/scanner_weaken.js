import { SHORT_DELAY, JOB_QUEUE } from "./constants.js"
import { get_core_servers } from "./utils.js"

/** @param {NS} ns */
export async function main(ns) {
  ns.disableLog("sleep")
  const host_server = ns.getHostname()
  const target_servers = get_core_servers()

  while (true) {
    await ns.sleep(1000)

    ns.print("Let's go again")
    for (const target_server of target_servers) {
      await find_target(ns, host_server, target_server)
      ns.print("Sleeping")
      await ns.sleep(2000)
      ns.print("Still alive")
    }
  }
}

async function find_target(ns, host_server, target_server) {
  await ns.sleep(2000)

  ns.print("Processing server " + target_server)
  if (target_server == ".") {
    ns.print("  Skipping dot server")
    return
  }

  if (!ns.serverExists(target_server)) {
    ns.print("  Server " + target_server + " not found")
    return
  }

  if (!ns.hasRootAccess(target_server)) {
    ns.print("  No root access to " + target_server)
    return
  }

  const lock_files = ns.ls(host_server, ".weaken." + target_server + ".lock.txt")
  if (lock_files.length > 0) {
    ns.print("  Lock file found for " + target_server)
    return
  }

  const threads_needed = Math.floor((ns.getServerSecurityLevel(target_server) -
    ns.getServerMinSecurityLevel(target_server)) * 20)
  if (threads_needed <= 0) {
    return
  }

  ns.print("  Setting up job data for " + target_server)
  const job_data = target_server + ",weaken," + threads_needed

  const ok = ns.tryWritePort(JOB_QUEUE, job_data)

  if (!ok) {
    ns.print("  Unable to write to job queue")
    return
  }

  ns.print("  Added data to the job queue: " + job_data)
}
