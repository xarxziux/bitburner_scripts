/** @param {NS} ns */
export async function main(ns) {
  const server = ns.getHostname()
  const min_security = ns.getServerMinSecurityLevel(server)

  while (ns.getServerSecurityLevel(server) > min_security) {
    await ns.weaken(server)
  }

  ns.tprint("Security of server " + server + " is at minimum level")
}