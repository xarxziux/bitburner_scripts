import { SHORT_DELAY, JOB_QUEUE } from "./constants.js"
import { get_core_servers } from "./utils.js"

const THRESHOLD = 20

/** @param {NS} ns */
export async function main(ns) {
  ns.disableLog("sleep")
  const host_server = ns.getHostname()
  const target_servers = get_core_servers()

  while (true) {
    for (const target_server of target_servers) {
      await find_target(ns, host_server, target_server)
    }
  }
}

/** 
 * @param {NS} ns 
 * @param {string} host_server
 * @param {string} target_server 
 */
async function find_target(ns, host_server, target_server) {
  await ns.sleep(2000)

  ns.print("Processing server " + target_server)
  if (!ns.serverExists(target_server)) {
    ns.print("  Server " + target_server + " not found")
    return
  }

  if (!ns.hasRootAccess(target_server)) {
    ns.print("  No root access to " + target_server)
    return
  }

  const lock_files = ns.ls(host_server, ".grow." + target_server + ".lock.txt")
  if (lock_files.length > 0) {
    ns.print("  Lock file found for " + target_server)
    return
  }

  const max = ns.getServerMaxMoney(target_server)
  const curr = ns.getServerMoneyAvailable(target_server)
  const multiplier = curr == 0 ? Infinity : max / curr
  const requested_threads =  (multiplier > THRESHOLD 
    ? 100 
    : Math.ceil(ns.growthAnalyze(target_server, multiplier)))

  ns.print("  Setting up job data for " + target_server)
  const job_data = target_server + ",grow," + threads_needed

  const ok = ns.tryWritePort(JOB_QUEUE, job_data)

  if (!ok) {
    ns.print("  Unable to write to job queue")
    return
  }

  ns.print("  Added data to the job queue: " + job_data)
}