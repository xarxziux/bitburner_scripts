import { get_servers } from "./get_servers.js"

/** @param {NS} ns */
export async function main(ns) {
  const servers = get_servers()

  servers.forEach((server) => {
    if (ns.fileExists("target.txt", server) || ns.fileExists("hack_lock.txt", server)) {
      ns.tprint("Server " + server + " is being targetted")
    }
  })
}