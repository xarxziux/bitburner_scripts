import { SHORT_DELAY, LOCK_QUEUE } from "./constants.js"

/** @param {NS} ns */
export async function main(ns) {
  ns.disableLog("sleep")

  while (true) {
    await ns.sleep(SHORT_DELAY)

    const lock_file = "" + ns.readPort(LOCK_QUEUE)
    if (lock_file == "NULL PORT DATA") {
      continue
    }

    if (!ns.fileExists(lock_file)) {
      ns.print("Invalid file name found on lock queue: " + lock_file)
      continue
    }

    ns.print("Deleting " + lock_file)
    if (!ns.rm(lock_file)) {
      ns.print("File deletion failed")
    }
  }
}