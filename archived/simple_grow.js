/** @param {NS} ns */
export async function main(ns) {
  const server = ns.args[0]
  const threads = ns.args[1]
  await ns.grow(server, { stock: true, threads: threads })
}