/** @param {NS} ns */
export async function main(ns) {
  const server = ns.args[0]
  const threads = ns.args[1]

  ns.tprint("Grow time = " + ns.getGrowTime(server))
  const rate = await ns.grow(server, { stock: true, threads: threads })
  ns.tprint("Actual growth rate = " + rate)
}