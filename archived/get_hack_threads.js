/** @param {NS} ns */
export async function main(ns) {
  const server = ns.args[0]
  const curr_funds = ns.getServerMoneyAvailable(server)
  const needed_threads = Math.ceil(
    ns.hackAnalyzeThreads(server, curr_funds))

  ns.tprint("Needed threads: " + needed_threads)
}