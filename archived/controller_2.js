/** @param {NS} ns */
export async function main(ns) {
  const host_server = ns.getHostname()
  const target_server = ns.args[0]

  if (!ns.serverExists(target_server)) {
    ns.tprint("Invalid server name: " + target_server)
    return
  }

  const max_ram = ns.getServerMaxRam(host_server)
  const max_funds = ns.getServerMoneyAvailable(target_server)

  if (max_funds == 0) {
    ns.tprint("Cannot hack " + target_server + ".  No funds available ")
    return
  }
  const min_funds = max_funds / 10
  const min_security = ns.getServerMinSecurityLevel(target_server)

  while (true) {
    await ns.sleep(1000)

    const required_weaken_threads = Math.floor((ns.getServerSecurityLevel(target_server) - min_security) * 20)

    if (required_weaken_threads > 0) {
      const available_threads = Math.floor((max_ram - ns.getServerUsedRam(host_server)) / 1.75)
      const actual_threads = Math.min(available_threads, required_weaken_threads)

      if (actual_threads > 0) {
        const pid = ns.run("lock_weaken.js", actual_threads, host_server, target_server, actual_threads)
        if (pid == 0) {
          ns.print("Failed to start weaken script")
        } else {
          ns.print("Started weaken script")
          post_lock_file(ns, host_server, target_server, "weaken")
        }
      }
    }

    const curr_funds = ns.getServerMoneyAvailable(target_server)
    const grow_mult = curr_funds == 0 ? 10 : max_funds / curr_funds
    const required_grow_threads = Math.ceil(ns.growthAnalyze(target_server, Math.max(grow_mult, 1)))

    if (required_grow_threads > 0) {
      const available_threads = Math.floor((max_ram - ns.getServerUsedRam(host_server)) / 1.75)
      const actual_threads = Math.min(available_threads, required_grow_threads)

      if (actual_threads > 0) {
        const pid = ns.run("lock_grow.js", actual_threads, host_server, target_server, actual_threads)


        if (pid == 0) {
          ns.print("Failed to start grow script")
        } else {
          ns.print("Started grow script")
          post_lock_file(ns, host_server, target_server, "grow")
        }
      }
    }

    const curr_funds_1 = ns.getServerMoneyAvailable(target_server)
    const is_weak = ns.getServerSecurityLevel(target_server) * 0.95 <= min_security
    const is_full = curr_funds_1 >= max_funds * 0.95

    if (is_weak && is_full) {
      const required_threads = Math.floor(ns.hackAnalyzeThreads(target_server, (curr_funds_1 - min_funds)))
      const available_threads = Math.floor((max_ram - ns.getServerUsedRam(host_server)) / 1.7)
      const actual_threads = Math.min(available_threads, required_threads)

      if (actual_threads > 0) {
        const pid = ns.run("lock_hack.js", actual_threads, host_server, target_server, actual_threads)

        if (pid == 0) {
          ns.print("Failed to start hack script")
        } else {
          ns.print("Started hack script")
          post_lock_file(ns, host_server, target_server, "hack")
        }
      }
    }
  }
}

/** 
 * @param {NS} ns 
 * @param (string) host_server
 * @param (string) target_server
 * @param (string) job
 */
function post_lock_file(ns, host_server, target_server, job) {
  const lock_file = host_server + "." + job + "." + target_server + ".lock.txt"
  ns.write(lock_file, "1", "w")
  ns.scp(lock_file, "home")
  ns.rm(lock_file)
}