/** @param {NS} ns */
export async function main(ns) {
  const host_server = ns.args[0]
  const target_server = ns.args[1]

  if (!ns.serverExists(host_server)) {
    ns.tprint("Unrecognised host server " + host_server)
    return
  }

  if (!ns.serverExists(target_server)) {
    ns.tprint("Unrecognised target server " + target_server)
    return
  }

  let ok = ns.scp(["controller_2.js", "lock_weaken.js", "lock_grow.js", "lock_hack.js", "constants.js"], host_server)
  if (!ok) {
    ns.tprint("Failed to upload host files")
    return
  }

  ns.write("target.txt", host_server, "w")

  ok = ns.scp("target.txt", target_server)
  if (!ok) {
    ns.tprint("Failed to upload target file")
    return
  }

  ns.rm("target.txt")
  ns.write("all_targets.txt", "{host: " + host_server + ", target: " + target_server + "}, ")
  ns.exec("controller_2.js", host_server, 1, target_server)
}