/** @param {NS} ns */
export async function main(ns) {
  ns.disableLog("getServerUsedRam")
  ns.disableLog("sleep")
  ns.disableLog("getServerSecurityLevel")
  ns.disableLog("getServerMoneyAvailable")
  ns.disableLog("getHackingLevel")
  ns.disableLog("getServerMaxRam")
  ns.disableLog("getServerMaxMoney")
  ns.disableLog("getServerRequiredHackingLevel")
  ns.disableLog("run")
  //ns.disableLog("")

  const host_server = ns.getHostname()
  const target_server = ns.args[0]

  if (!ns.serverExists(target_server)) {
    ns.tprint("Invalid server param")
    ns.tprint("Usage: thread_controller <target_server>")
    return
  }

  const max_ram = ns.getServerMaxRam(host_server)
  const min_security = ns.getServerMinSecurityLevel(target_server)
  const max_money = ns.getServerMaxMoney(target_server)
  const min_hack_level = ns.getServerRequiredHackingLevel(target_server)

  while (true) {
    const free_ram = max_ram - ns.getServerUsedRam(host_server)
    const security_level = ns.getServerSecurityLevel(target_server)
    const funds = ns.getServerMoneyAvailable(target_server)
    const hacking_level = ns.getHackingLevel()
    const available_threads = Math.floor(free_ram / 1.75)
    const available_hack_threads = Math.floor(free_ram / 1.70)

    ns.print("Current security level: " + security_level + ", minimum security: " + min_security)
    ns.print("Current money: " + funds + ", max money: " + max_money)
    ns.print("Hacking level: " + hacking_level + ", min hack level: " + min_hack_level)

    if (available_threads > 0 && security_level > min_security) {
      ns.print("Calling weaken() for " + target_server)
      ns.run("simple_weaken.js", available_threads, target_server, available_threads)
    } else if (available_threads > 0 && funds < max_money) {
      ns.print("Calling grow() for " + target_server)
      ns.run("simple_grow.js", { stock: true, threads: available_threads }, target_server, available_threads)
    } else if (available_hack_threads > 0 && hacking_level >= min_hack_level) {
      ns.print("Calling hack() for " + target_server)
      ns.run("simple_hack.js", available_hack_threads, target_server, available_hack_threads)
    }

    await ns.sleep(1000 * 10)
  }
}