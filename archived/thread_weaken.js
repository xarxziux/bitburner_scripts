/** @param {NS} ns */
export async function main(ns) {
  const server = ns.args[0]
  const threads = ns.args[1]
  const min_security = ns.getServerMinSecurityLevel(server)

  while (ns.getServerSecurityLevel > min_security) {
    await ns.weaken(server, { threads: threads })
  }

  //ns.tprint("Security level on server " + server + " has been reduced")
}