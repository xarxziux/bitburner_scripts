/** @param {NS} ns */
export async function main(ns) {
  const server = ns.args[0]
  const threads = ns.args[1]
  const loot = await ns.hack(server, { threads: threads })
  ns.write("hack_log.txt", "{target_server: " + server + ", loot: " + loot + "}, ", "a")
}