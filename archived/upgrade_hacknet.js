/** @param {NS} ns */
export async function main(ns) {
  while (true) {
    let node = 0
    const nodes = ns.hacknet.numNodes();
    const funds = ns.getServerMoneyAvailable("home")

    const new_node_cost = ns.hacknet.getPurchaseNodeCost()

    if (new_node_cost < funds) {
      ns.print("Purchasing new node")
      ns.hacknet.purchaseNode();
    }

    for (node = 0; node < nodes; node++) {
      if (ns.hacknet.getLevelUpgradeCost(node) < funds) {
        ns.print("Upgrading level for node " + node)
        ns.hacknet.upgradeLevel(node)
      }

      if (ns.hacknet.getRamUpgradeCost(node) < funds) {
        ns.print("Upgrading RAM for node " + node)
        ns.hacknet.upgradeRam(node)
      }

      if (ns.hacknet.getCoreUpgradeCost(node) < funds) {
        ns.print("Upgrading cores for node " + node)
        ns.hacknet.upgradeCore(node)
      }
    }

    await min_sleep(ns, 1)
  }
}

async function min_sleep(ns, mins) {
  if (mins > 0) {
    ns.print("Resuming in " + mins + " minutes")
    await ns.sleep(60000)
    await min_sleep(ns, mins - 1)
  }
}
