// Imported from VSCode

/** @param {NS} ns */
export async function main(ns) {
  const host_server = ns.getHostname()
  const max_ram = ns.getServerMaxRam(host_server)
  const free_ram = max_ram - ns.getServerUsedRam(host_server)
  ns.tprint("Free RAM = " + free_ram)
}