import { SERVER_PORT, MESSAGE_PORT } from "./constants.js"
import { get_servers } from "./utils.js"

/** @param {NS} ns */
export async function main(ns) {
  const servers = get_servers()
  const port = ns.getPortHandle(SERVER_PORT)
  port.clear()

  servers.forEach((server) => {
    port.write(server)
  })

  ns.writePort(MESSAGE_PORT, "Server port populated")
}