/** @param {NS} ns */
export async function main(ns) {
  const threads = ns.args[0]

  if (threads == undefined) {
    ns.tprint("Usage: ./grind_xp.js <threads>")
    return
  }

  while(true) {
    await ns.hack("darkweb", {threads: threads})
  }
}