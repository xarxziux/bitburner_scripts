/** @param {NS} ns */
export async function main(ns) {
  const server = ns.args[0]
  const threads = ns.args[1]
  const lock_file = "weaken_" + server + ".lock.txt"

  await ns.weaken(server, { stock: true, threads: threads })
  ns.write(lock_file)
}