import { get_servers, get_free_ram, get_sorted_servers } from 'utils.js'

/** @param {NS} ns */
export async function main(ns) {
  while (true) {
    const host_servers = ns.getPurchasedServers()
    host_servers.push("home")
    const target_servers = get_servers()

    ns.getWeakenTime()

    await ns.sleep(1000)
  }
}

function has_weaken_lock(server) {
  return ns.fileExists("weaken_" + server + ".lock.txt")
}

function has_grow_lock(server) {
  return ns.fileExists("grow_" + server + ".lock.txt")
}

function has_hack_lock(server) {
  return ns.fileExists("hack_" + server + ".lock.txt")
}

function find_most_ram(servers) {
  const answer = {
    server: "",
    free_ram: 0
  }

  servers.forEach((server) => {
    const free_ram = get_free_ram(server)
    if (free_ram > answer.ram) {
      answer.server = server,
        answer.free_ram = free_ram
    }
  })

  return answer
}

