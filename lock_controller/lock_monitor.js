import { get_servers } from "./utils.js"

/** @param {NS} ns */
export async function main(ns) {
  const servers = ns.getPurchasedServers()

  while (true) {
    servers.forEach((server) => {
      const text_files = ns.ls(server, ".lock.txt")
      text_files.forEach((file) => {
        ns.tprint("Found " + file + " on " + server)
        ns.rm(file, server)
        ns.rm(file)
      })
    })

    await ns.sleep(1000)
  }
}