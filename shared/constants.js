export const MESSAGE_QUEUE = 1
export const LOCK_QUEUE_SET = 2
export const LOCK_QUEUE_UNSET = 3
export const JOB_QUEUE = 4
export const WORKER_QUEUE = 5

export const SHORT_DELAY = 1000
export const MEDIUM_DELAY = 5000
export const LONG_DELAY = 20000