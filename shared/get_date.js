/** @param {NS} ns */
export function get_date(ns) {
  return (ns.tFormat(Date.now() - ns.getResetInfo().lastAugReset)
    .replace(" days ", ":")
    .replace(" day ", ":")
    .replace(" hours ", ":")
    .replace(" hour ", ":")
    .replace(" minutes ", ":")
    .replace(" minute ", ":")
    .replace(" seconds", "")
    .replace(" second", ""))
}
