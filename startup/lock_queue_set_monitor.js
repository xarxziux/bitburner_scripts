import { SHORT_DELAY, LOCK_QUEUE_SET } from "shared/constants.js"
import { get_date } from "shared/get_date.js"

/** @param {NS} ns */
export async function main(ns) {
  ns.disableLog("sleep")
  const host = ns.getHostname()

  while (true) {
    await ns.sleep(SHORT_DELAY)

    const file = "" + ns.readPort(LOCK_QUEUE_SET)
    if (file == "NULL PORT DATA") {
      continue
    }

    const breakdown = file.split(" ")
    let file_name = ""
    let file_data = ""

    if (breakdown.length == 1) {
      file_name = breakdown[0]
      file_data = 1
    } else if (breakdown.length == 2) {
      file_name = breakdown[0]
      file_data = breakdown[1]
    } else {
      ns.print("Unable to parse " + file)
      continue
    }

    if (ns.fileExists(file_name, "home")) {
      ns.print("Lock file already created: " + file_name)
      continue
    }

    ns.print(get_date(ns) + ": Adding " + file_name)
    ns.write(file_name, file_data, "w")

    if (host != "home") {
      ns.scp(file_name, "home")
      ns.rm(file_name)
    }
  }
}