import { SHORT_DELAY, LOCK_QUEUE_UNSET } from "shared/constants.js"
import { get_date } from "shared/get_date.js"

/** @param {NS} ns */
export async function main(ns) {
  ns.disableLog("sleep")

  while (true) {
    await ns.sleep(SHORT_DELAY)

    const lock_file = "" + ns.readPort(LOCK_QUEUE_UNSET)
    if (lock_file == "NULL PORT DATA") {
      continue
    }

    if (!ns.fileExists(lock_file, "home")) {
      ns.print("Invalid file name found on lock queue: " + lock_file)
      continue
    }

    ns.print(get_date(ns) + ": Deleting " + lock_file)
    if (!ns.rm(lock_file, "home")) {
      ns.print("File deletion failed")
    }
  }
}
