/** @param {NS} ns */
export async function main(ns) {
  ns.disableLog("getServerMoneyAvailable")
  ns.disableLog("sleep")

  while (true) {
    const total_nodes = ns.hacknet.numNodes();
    const funds = ns.getServerMoneyAvailable("home")
    const lowest = [ns.hacknet.getPurchaseNodeCost(), -1, -1]

    if (total_nodes >= 10) {
      lowest[0] = Infinity
    } else {
      if (ns.hacknet.getPurchaseNodeCost() < funds) {
        ns.hacknet.purchaseNode()
        continue
      }
    }

    if (total_nodes == 0) {
      ns.print("No nodes found.  Try again when you have enough funds.")
      return
    }

    for (let current_node = 0; current_node < total_nodes; current_node++) {
      if (ns.hacknet.getLevelUpgradeCost(current_node) < lowest[0]) {
        lowest[0] = ns.hacknet.getLevelUpgradeCost(current_node)
        lowest[1] = 0
        lowest[2] = current_node
      }
      if (ns.hacknet.getRamUpgradeCost(current_node) < lowest[0]) {
        lowest[0] = ns.hacknet.getRamUpgradeCost(current_node)
        lowest[1] = 1
        lowest[2] = current_node
      }
      if (ns.hacknet.getCoreUpgradeCost(current_node) < lowest[0]) {
        lowest[0] = ns.hacknet.getCoreUpgradeCost(current_node)
        lowest[1] = 2
        lowest[2] = current_node
      }
    }

    if (lowest[0] == Infinity) {
      return
    }

    if (lowest[0] < funds) {
      switch (lowest[1]) {
        case 0:
          ns.print("Upgrading level for node " + lowest[2])
          ns.hacknet.upgradeLevel(lowest[2])
          break
        case 1:
          ns.print("Upgrading RAM for node " + lowest[2])
          ns.hacknet.upgradeRam(lowest[2])
          break
        case 2:
          ns.print("Upgrading cores for node " + lowest[2])
          ns.hacknet.upgradeCore(lowest[2])
      }
    } else {
      ns.print("Out of funds.  Sleeping...")

      if (lowest[0] == Infinity) {
        break
      }

      while (lowest[0] > ns.getServerMoneyAvailable("home")) {
        await ns.sleep(10000)
      }
    }
  }
}