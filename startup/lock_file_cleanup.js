import { get_date } from "shared/get_date.js"

/** @param {NS} ns */
export async function main(ns) {
  ns.disableLog("sleep")

  //const lockRX = /^(?<host>.*)\.(?<job>grow|hack|weaken)\.(?<target>.*)\.lock\.txt$/

  while (true) {
    await ns.sleep(10000)

    const files = ns.ls("home", ".lock.txt")

    for (const file of files) {
      await ns.sleep(200)

      const data = "" + ns.read(file)

      if (data.slice(0, 4) != "pid:") {
        continue
      }

      ns.print(get_date(ns) + ": Parsing " + data + " from file " + file)

      const pid = Number(data.slice(4))

      if (Number.isNaN(pid)) {
        ns.print("  PID parsing failed for " + file)
        continue
      }

      if (!ns.isRunning(pid)) {
        ns.print("  PID " + pid + " not found.  Deleting file " + file)
        ns.rm(file, "home")
      } else {
        ns.print("  Script with pid " + pid + " is still running")
      }
    }
  }
}