import { MESSAGE_QUEUE } from "shared/constants.js"
import { get_date } from "shared/get_date.js"

/** @param {NS} ns */
export async function main(ns) {
  ns.disableLog("sleep")

  while (true) {
    const data = ns.readPort(MESSAGE_QUEUE)

    if (data == "NULL PORT DATA") {
      await ns.sleep(100)
      continue
    }

    ns.print(get_date(ns) + ": " + data)
  }
}
