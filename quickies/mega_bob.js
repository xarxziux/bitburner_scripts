/** @param {NS} ns */
export async function main(ns) {
  for (let i = 0; i < 25; i++) {
    await ns.sleep(500)
    const server = "bob-" + i
    
    if (!ns.serverExists(server)) {
      ns.purchaseServer(server, 1048576)
    }

    ns.scp("startup/grind_n00dles.js", server)
    ns.exec("startup/grind_n00dles.js", server, 615000, 615000)
  }
}