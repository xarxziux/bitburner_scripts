/** @param {NS} ns */
export async function main(ns) {
  if (ns.args.length == 0) {
    ns.tprint("Usage: start_grind.js <server1>...")
    return
  }

  for (let i = 0; i < ns.args.length; i++) {
    const server = ns.args[i]
    ns.scp("startup/grind_n00dles.js", server)
    ns.exec("startup/grind_n00dles.js", server, 9, 9)
  }
}