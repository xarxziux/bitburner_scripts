import { get_core_servers } from "shared/utils.js"

/** @param {NS} ns */
export async function main(ns) {
  const servers = get_core_servers()

  for (server of servers) {
    const w = ns.getWeakenTime(server)
    const g = ns.getGrowTime(server)
    const h = ns.getHackTime(server)

    ns.tprint("Weaken to hack = " + w / h)
    ns.tprint("Weaken to grow = " + w / g)
  }
}