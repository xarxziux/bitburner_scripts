/** @param {NS} ns */
export async function main(ns) {
  const threads = ns.args[0]
  const server = ns.args[1]

  if (threads == undefined) {
    ns.tprint("Usage: ./grind_server.js <threads> <server>")
    return
  }

  if (server == undefined) {
    ns.tprint("Usage: ./grind_server.js <threads> <server>")
    return
  }

  ns.tprint("Grinding " + server + " with " + threads + " threads")

  while(true) {
    await ns.hack(server, {threads: threads})
  }
}