/** @param {NS} ns */
export async function main(ns) {
  ns.upgradePurchasedServer("megacorp-hack", 1048576)
  ns.upgradePurchasedServer("ecorp-hack", 1048576)
}