/** @param {NS} ns */
export async function main(ns) {
  for (let i = 1; i < ns.args.length; i++) {
    ns.mv("home", ns.args[i], ns.args[0] + "/" + ns.args[i])
  }
}