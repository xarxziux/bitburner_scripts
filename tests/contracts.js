import { get_core_servers } from "utils.js"

/** @param {NS} ns */
export async function main(ns) {
  const servers = get_core_servers()
  const contract_types = ns.codingcontract.getContractTypes()

  for (const type of contract_types){
    ns.tprint(type)
  }
}