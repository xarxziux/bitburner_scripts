import { get_servers, sortByLargestValue, format_number } from "./utils.js"

/** @param {NS} ns */
export async function main(ns) {
  const servers = get_servers()
  const min = ns.args[0] || 0
  const max = ns.args[1] || Infinity
  const items = []

  if (ns.serverExists(min)) {
    ns.tprint("Max money: " + ns.getServerMaxMoney(min) + ", current money: " + ns.getServerMoneyAvailable(min))
    return
  }

  ns.tprint("min: " + min + ", max: " + max)

  servers.forEach((server) => {
    if (!ns.hasRootAccess(server)) {
      return
    }

    if (ns.getServerRequiredHackingLevel(server) > ns.getHackingLevel()) {
      return
    }

    const funds = ns.getServerMaxMoney(server)

    if (funds >= min && funds <= max) {
      // ns.tprint(server + ": " + Math.floor(money / 1000000) + "M" + attacker)
      items.push({ server: server, value: funds, attacker: ns.fileExists("target.txt", server) })
    }
  })

  items.sort(sortByLargestValue)

  items.forEach(item => {
    if (item.attacker) {
      ns.tprint(item.server + ": " + format_number(item.value) + " *")
    } else {
      ns.tprint(item.server + ": " + format_number(item.value))
    }
  })
}
