/** @param {NS} ns */
export async function main(ns) {
  const nns = ns
  const fnBody = "z(x, y)"
  const fnArg1 = "x"
  const fnArg2 = "y"
  const fnArg3 = "z"
  
  const fn = new Function(fnArg1, fnArg2, fnArg3, fnBody)
  fn("johnson-ortho", ns.hack) 
}