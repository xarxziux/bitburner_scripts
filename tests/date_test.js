import { get_date } from "get_date.js"

/** @param {NS} ns */
export async function main(ns) {
  //ns.tprint(ns.tFormat(Date.now() - ns.getResetInfo().lastAugReset))
  //const info = ns.getResetInfo()
  //ns.tprint(ns.tFormat(info.lastAugReset))
  ns.tprint(get_date(ns))
}