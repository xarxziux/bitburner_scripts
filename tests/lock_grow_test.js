/** @param {NS} ns */
export async function main(ns) {
  const server = ns.args[0]
  const threads = ns.args[1]
  const lock_file = "lock_grow_" + server + ".txt"

  ns.write(lock_file, "1", "w")
  await ns.grow(server, { stock: true, threads: threads })
  ns.write(lock_file, "0", "w")
}