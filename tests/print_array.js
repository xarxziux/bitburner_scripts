/** @param {NS} ns */
export function main(ns) {
  const arr = [1, 2, 3, 4]
  ns.tprint("arr = " + arr.join(" "))
}