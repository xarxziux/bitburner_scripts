/** @param {NS} ns */
export async function main(ns) {
  const max_threads = ns.args[0]

  while (true) {
    const job_data = ns.readPort(1).split("_")
    await ns.hack(job_data[0], { threads: job_data[1] })
  }
}