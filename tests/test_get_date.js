import { get_date } from "get_date.js"

/** @param {NS} ns */
export async function main(ns) {
  ns.tprint(get_date(ns))
}