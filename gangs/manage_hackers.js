const threshold = 1.5

/** @param {NS} ns */
export async function main(ns) {
  ns.disableLog("sleep")

  while (true) {
    const members = ns.gang.getMemberNames()

    for (const member of members) {
      maybe_ascend(ns, member)
      await ns.sleep(200)
    }

    ns.print("Sleeping...")
    await ns.sleep(60000)
  }
}

/** 
 * @param {NS} ns 
 * @param {string} member
 */
function maybe_ascend(ns, member) {
  ns.print("Processing " + member)

  const asc = ns.gang.getAscensionResult(member)
  if (asc == undefined) {
    ns.print("  Unable to ascend " + member)
    return
  }

  if (asc.hack >= threshold) {
    ns.print("  Ascending " + member)
    ns.gang.ascendMember(member)
  }
}
