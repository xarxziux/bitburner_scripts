/** @param {NS} ns */
export async function main(ns) {
  ns.disableLog("sleep")

  let old_karma = (Math.round(ns.heart.break() * 100)) / 100
  ns.print("Starting karma = " + old_karma)

  while (true) {
    await ns.sleep(60000)

    const karma = (Math.round(ns.heart.break() * 100)) / 100
    const gain = (Math.round((karma - old_karma) * 100)) / 100
    ns.print("Karma = " + karma + ", karma gained = " + gain)
    old_karma = karma
  }
}
