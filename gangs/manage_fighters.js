const threshold = 1.5
let focus = "Terrorism"

/** @param {NS} ns */
export async function main(ns) {
  ns.disableLog("sleep")
  ns.disableLog("gang.ascendMember")
  ns.disableLog("gang.purchaseEquipment")
  ns.disableLog("gang.setMemberTask")

  const file_focus = ns.read("combat_gang_focus.txt")
  if (file_focus == "Human Trafficking" || file_focus == "Territory Warfare"){
    focus = file_focus
  }

  ns.print("Focusing on " + focus)

  while (true) {
    await ns.sleep(200)

    const members = ns.gang.getMemberNames()
    for (const member of members) {
      update_member(ns, member)
      continue
    }

    ns.print("Sleeping...")
    await ns.sleep(60000)
  }
}

/** 
 * @param {NS} ns
 * @param {string} member
 */
function update_member(ns, member) {
  ns.print("Processing " + member)

  const asc = ns.gang.getAscensionResult(member)
  if (asc != undefined) {
    if (asc.str >= threshold) {
      ascend(ns, member)
    }
  }

  const member_data = ns.gang.getMemberInformation(member)
  const task = member_data.task

  if (task=="Unassigned"){
    ns.gang.setMemberTask(member, "Train Combat")
    return
  }

  if (task == "Vigilante Justice") {
    ns.print("  " + member + " is already working on Vigilante Justice")
    return
  }

  if (task == "Train Combat") {
    const terror_score = member_data.str + member_data.def + member_data.dex + member_data.cha
    if (terror_score >= 700) {
      ns.gang.setMemberTask(member, focus)
    }
  }
}

/** 
 * @param {NS} ns
 * @param {string} member
 */
function ascend(ns, member) {
  ns.print("  Ascending " + member)
  const ok = ns.gang.ascendMember(member)
  if (!ok) {
    ns.print("  Ascension failed")
    return
  }

  ns.print("  Ascension succeeded")
  equip(ns, member)
  ns.gang.setMemberTask(member, "Train Combat")
}

/** 
 * @param {NS} ns
 * @param {string} member
 */
function equip(ns, member) {
  const gear = [
    "Baseball Bat",
    "Katana",
    "Glock 18C"
  ]

  for (const item of gear) {
    const ok = ns.gang.purchaseEquipment(member, item)
    if (ok) {
      ns.print("  Equipped " + member + " with " + item)
    } else {
      ns.print("  Failed to equip " + member + " with " + item)
    }
  }
}