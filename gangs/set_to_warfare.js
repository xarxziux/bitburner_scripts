/** @param {NS} ns */
export async function main(ns) {
  const members = ns.gang.getMemberNames()

  ns.write("combat_gang_focus.txt", "Territory Warfare", "w")

  for (const member of members) {
    const task = ns.gang.getMemberInformation(member).task

    if (task == "Human Trafficking" || task == "Terrorism") {
      const ok = ns.gang.setMemberTask(member, "Territory Warfare")
      if (!ok) {
        ns.tprint("Failed to set " + member + " to territory warfare")
      }
    }
  }
}
