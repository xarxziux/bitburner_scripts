/** @param {NS} ns */
export async function main(ns) {
  const members = ns.gang.getMemberNames()

  for (const member of members){
    ns.tprint("Processing " + member)

    if (ns.gang.getMemberInformation(member).task == "Ethical Hacking"){
      ns.tprint("  " + member + " set to etchical hacking")
      continue
    }

    ns.tprint("  Setting " + member + " to money laundering")
    const ok = ns.gang.setMemberTask(member, "Money Laundering")

    if (!ok) {
      ns.tprint("  Failed to set " + member + " to money laundering")
    } else {
      ns.print("  Set")
    }
  }
}
