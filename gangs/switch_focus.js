/** @param {NS} ns */
export async function main(ns) {
  const members = ns.gang.getMemberNames()

  for (const member of members){
    const ok = ns.gang.setMemberTask(member, "Human Trafficking")

    if (!ok) {
      ns.tprint("Failed to set " + member + " to trafficking")
    }
  }
}