/** @param {NS} ns */
export async function main(ns) {
  const members = ns.gang.getMemberNames()

  ns.write("combat_gang_focus.txt", "Human Trafficking", "w")

  for (const member of members) {
    const task = ns.gang.getMemberInformation(member).task

    if (task == "Terrorism" || task == "Territory Warfare") {
      const ok = ns.gang.setMemberTask(member, "Human Trafficking")
      if (!ok) {
        ns.tprint("Failed to set " + member + " to trafficking")
      }
    }
  }
}
