/** @param {NS} ns */
export async function main(ns) {
  const members = ns.gang.getMemberNames()

  for (const member of members) {
    if (ns.gang.getMemberInformation(member).task == "Ethical Hacking") {
      continue
    }

    const ok = ns.gang.setMemberTask(member, "Cyberterrorism")

    if (!ok) {
      ns.tprint("Failed to set " + member + " to cyberterrorism")
    }
  }
}
