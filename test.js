/** @param {NS} ns */
export async function main(ns) {
  show_weaken_threads(ns)
  //ns.tail
}

function show_weaken_threads(ns) {
  const server = ns.args[0]

  if (server == undefined) {
    ns.tprint("Usage: test.js <server>")
  }

  const curr_security = ns.getServerSecurityLevel(server) 
  const min_security = ns.getServerMinSecurityLevel(server)
  const threads_needed = (curr_security - min_security) * 20
  ns.tprint("Current security: " + curr_security + ", min security: " + min_security + 
    ", threads needed: " + threads_needed)
}