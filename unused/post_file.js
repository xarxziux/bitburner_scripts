import { MESSAGE_QUEUE } from "constants.js"

/** @param {NS} ns */
export async function main(ns) {
  const file = ns.args[0] || ""
  if (file == "") {
    ns.tryWritePort(MESSAGE_QUEUE, "Post file called with no argument")
  }

  if (ns.fileExists(file)) {
    ns.tryWritePort(MESSAGE_QUEUE, "Duplicate file detected: " + file)
  }

  ns.write(file, "1", "w")
}