/** @param {NS} ns */
export async function main(ns) {
  while (true) {
    const cost = ns.hacknet.getPurchaseNodeCost()
    const funds = ns.getServerMoneyAvailable("home")

    if (cost < funds) {
      ns.print("Attempting to purchase node")
      const node = ns.hacknet.purchaseNode()

      if (node == -1) {
        ns.print("Unable to purchase node")
      } else {
        ns.print("Purchased node " + node)
      }
    } else {
      ns.print("Not enough money yet.  Sleeping...")
    }

    await ns.sleep(1000 * 60 * 10)
  }
}
