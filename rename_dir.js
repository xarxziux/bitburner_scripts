/** @param {NS} ns */
export async function main(ns) {
  const files = [
    "clear_lock_files.js",
    "clear_target_files.js",
    "constants.js",
    "copy_host_files.js",
    "grow_controller.js",
    "hack_controller.js",
    "kill_controllers.js",
    "lock_grow.js",
    "lock_hack.js",
    "lock_weaken.js",
    "price_server.js",
    "reset_purchased_servers.js",
    "show_money.js",
    "show_ram.js",
    "start_self_host.js",
    "utils.js",
    "weaken_controller.js"
  ]

  for (const file of files) {
    ns.mv("home", "core_hack_scripts/" + file, "core/" + file)
  }

  ns.tprint("Done")
}