import { get_core_servers } from "shared/utils.js"

/** @param {NS} ns */
export async function main(ns) {
  const servers = get_core_servers()

  for (const server of servers) {
    if (ns.fileExists("target.txt", server)) {
      ns.tprint("Deleting target file from " + server)
      ns.rm("target.txt", server)
    }
  }
}