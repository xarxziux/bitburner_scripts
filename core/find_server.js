

/** @param {NS} ns */
export async function main(ns) {
  const target = ns.args[0]

  if (target == undefined) {
    ns.tprint("Usage: find_server.js <server>")
    return
  }

  if (!ns.serverExists(target)) {
    ns.tprint("Server " + target + " not found")
    return
  }

  find_home(ns, "", target)
}

function find_home(ns, root, current) {
  if (current == "home") {
    ns.tprint("home")
    return true
  }

  const links = ns.scan(current)

  if (links.length == 1 && links[0] == root) {
    return false
  }

  for (const link of links) {
    if (link == root) {
      continue
    }

    if (find_home(ns, current, link)) {
      ns.tprint(current)
      return true
    }
  }

  return false
}