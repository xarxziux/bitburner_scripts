/** @param {NS} ns */
export async function main(ns) {
  const servers = ns.getPurchasedServers()

  for (const host_server of servers) {
    await ns.sleep(1000)

    ns.tprint("Processing " + host_server)

    const target_server = host_server.replace("-hack", "")
    ns.killall(host_server)

    const weaken_lock_file = host_server + ".weaken." + target_server + 
      ".lock.txt"
    const grow_lock_file = host_server + ".grow." + target_server + 
      ".lock.txt"
    const hack_lock_file = host_server + ".hack." + target_server + 
      ".lock.txt"

    if (ns.fileExists(weaken_lock_file, "home")) {
      ns.rm(weaken_lock_file, "home")
    }
    
    if (ns.fileExists(grow_lock_file, "home")) {
      ns.rm(grow_lock_file, "home")
    }
    
    if (ns.fileExists(hack_lock_file, "home")) {
      ns.rm(hack_lock_file, "home")
    }    

    ns.run("start_hack.js", 1, host_server + "_" + target_server)
  }

  ns.print("Servers reset")
}