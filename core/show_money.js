import { get_core_servers, sort_by_largest_value, format_number } from "shared/utils.js"

/** @param {NS} ns */
export async function main(ns) {
  const servers = get_core_servers()
  const min = ns.args[0] || 0
  const max = ns.args[1] || Infinity
  const items = []

  ns.tprint("min = " + min)

  servers.forEach((server) => {
    if (!ns.hasRootAccess(server)) {
      return
    }

    const funds = ns.getServerMaxMoney(server)

    if (funds >= min && funds <= max) {
      // ns.tprint(server + ": " + Math.floor(money / 1000000) + "M" + attacker)
      items.push({
        server: server,
        value: funds,
        curr: ns.getServerMoneyAvailable(server),
        attacker:
          ns.serverExists(server + "-hack")
            ? "***"
            : ns.fileExists("target.txt", server)
              ? "*"
              : "",
        hack_warning: (ns.getServerRequiredHackingLevel(server) > ns.getHackingLevel()
          ? "N " + ns.getServerRequiredHackingLevel(server)
          : "")
      })
    }
  })

  items.sort(sort_by_largest_value)

  ns.tprint("Server              Max       Current  Status Can hack?")
  ns.tprint("======              ===       =======  ====== =========")

  items.forEach(item => {
    const max = "" + format_number(item.value)
    const curr = "" + format_number(item.curr)
    ns.tprint(item.server.padEnd(20) + max.padEnd(10) +
      curr.padEnd(10) + item.attacker.padEnd(6) + item.hack_warning)
  })

  ns.tprint("======              ===       =======  ====== =========")
}
