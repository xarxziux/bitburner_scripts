export function get_core_servers() {
  return [
    "ecorp",
    "megacorp",
    "b-and-a",
    "blade",
    "nwo",
    "clarkinc",
    "omnitek",
    "4sigma",
    "kuai-gong",
    "fulcrumtech",
    "fulcrumassets",
    "stormtech",
    "defcomm",
    "infocomm",
    "helios",
    "vitalife",
    "icarus",
    "univ-energy",
    "titan-labs",
    "microdyne",
    "taiyang-digital",
    "galactic-cyber",
    "aerocorp",
    "omnia",
    "zb-def",
    "applied-energetics",
    "solaris",
    "deltaone",
    "global-pharm",
    "nova-med",
    "zeus-med",
    "unitalife",
    "lexo-corp",
    "rho-construction",
    "alpha-ent",
    "aevum-police",
    "rothman-uni",
    "zb-institute",
    "summit-uni",
    "syscore",
    "catalyst",
    "the-hub",
    "computek",
    "netlink",
    "johnson-ortho",
    "n00dles",
    "foodnstuff",
    "sigma-cosmetics",
    "joesguns",
    "zer0",
    "nectar-net",
    "neo-net",
    "silver-helix",
    "hong-fang-tea",
    "harakiri-sushi",
    "phantasy",
    "max-hardware",
    "omega-net",
    "crush-fitness",
    "iron-gym",
    "millenium-fitness",
    "powerhouse-fitness",
    "snap-fitness",
    "run4theh111z",
    "I.I.I.I",
    "avmnite-02h",
    "CSEC",
    "The-Cave",
    "."
  ]
}

export function format_number(num) {
  if (num > 999999999999) {
    num = Math.floor(num / 100000000000)
    return (num / 10) + "Tri"
  }

  if (num > 999999999) {
    num = Math.floor(num / 100000000)
    return (num / 10) + "Bil"
  }

  if (num > 999999) {
    num = Math.floor(num / 100000)
    return (num / 10) + "Mil"
  }

  if (num > 999) {
    num = Math.floor(num / 100)
    return (num / 10) + "Tho"
  }

  return "" + num
}

export function sort_by_largest_value(x, y) {
  return (x.value >= y.value ? -1 : 1)
}

export function set_value_from_arg(val, def) {
  return (val == undefined ? def : val)
}
