/** @param {NS} ns */
export async function main(ns) {
  const threads = ns.args[0]

  if (threads == undefined) {
    ns.tprint("Usage: ./grind_n00dles.js <threads>")
    return
  }

  ns.tprint("Grinding n00dles with " + threads + " threads")

  while(true) {
    await ns.hack("n00dles", {threads: threads})
  }
}