import { get_core_servers } from "shared/utils.js"

/** @param {NS} ns */
export async function main(ns) {
  const servers = get_core_servers()

  for (const server of servers) {
    await ns.sleep(200)

    ns.tprint("Processing " + server)

    if (ns.getServerMaxRam(server) == 16) {
      ns.tprint("  Passed first test for " + server)

      if (ns.getServerUsedRam(server) == 0) {
        ns.tprint("  Passed second test for " + server)

        if (ns.hasRootAccess(server)) {
          ns.tprint("  Passed third test for " + server)
          ns.scp("core/grind_n00dles.js", server, "home")
          ns.exec("core/grind_n00dles.js", server, 9, 9)
        }
      }
    }
  }

  ns.tprint("start_grind script completed.")
}
