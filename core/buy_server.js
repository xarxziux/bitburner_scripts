import { format_number } from "shared/utils.js"

/** @param {NS} ns */
export async function main(ns) {
  const ram = ns.args[0]

  if (ram == undefined) {
    ns.tprint("Usage: buy_server.js <ram> <name1>...")
    return
  }

  for (let i = 1; i < ns.args.length; i++) {
    const cost = ns.getPurchasedServerCost(ram)

    if (cost == 0) {
      ns.tprint("Unable to buy a server with " + ram + " ram")
      return
    }

    if (cost > ns.getServerMoneyAvailable("home")) {
      ns.tprint("Not enough money available to buy server")
      return
    }

    const new_name = ns.purchaseServer(ns.args[i], ram)

    if (new_name == "") {
      ns.tprint("Failed to buy new server")
      return
    }

    ns.tprint("New server created: " + new_name)
  }

  ns.tprint("buy=server script completed")
}
