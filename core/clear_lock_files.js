/** @param {NS} ns */
export async function main(ns) {
  const files = ns.ls("home", ".lock.txt")

  files.forEach((file) => {
    ns.tprint("Deleting " + file)
    ns.rm(file)
  })
}