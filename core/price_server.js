import { format_number } from "shared/utils.js"

/** @param {NS} ns */
export async function main(ns) {
  ns.tprint("Max server RAM = " + ns.getPurchasedServerMaxRam())

  for (let i = 0; i < 21; i++) {
    const ram = Math.pow(2, i)
    ns.tprint("Cost for " + ram + "Gb server = " + format_number(ns.getPurchasedServerCost(ram)))
  }
}