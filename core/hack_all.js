import { get_core_servers } from "shared/utils.js"

/** @param {NS} ns */
export async function main(ns) {
  const servers = get_core_servers()
  let cracked_servers = []

  for (const server of servers) {
    await ns.sleep(1000)

    let open_ports = 0

    ns.tprint("Processing " + server)

    if (!ns.serverExists(server)) {
      ns.tprint("  Server " + server + " not found")
      continue
    }

    if (ns.hasRootAccess(server)) {
      ns.tprint("  Server " + server + " has already been compromised")
      continue
    }

    ns.tprint("  Attempting to open ports on " + server)

    if (ns.fileExists("BruteSSH.exe", "home")) {
      ns.brutessh(server)
      open_ports++
    }

    if (ns.fileExists("FTPCrack.exe", "home")) {
      ns.ftpcrack(server)
      open_ports++
    }

    if (ns.fileExists("relaySMTP.exe", "home")) {
      ns.relaysmtp(server)
      open_ports++
    }

    if (ns.fileExists("HTTPWorm.exe", "home")) {
      ns.httpworm(server)
      open_ports++
    }

    if (ns.fileExists("SQLInject.exe", "home")) {
      ns.sqlinject(server)
      open_ports++
    }

    if (open_ports > 0) {
      ns.tprint("  Successfully opened " + open_ports + " ports on server " + server)
      ns.tprint("  Required number of ports is " + ns.getServerNumPortsRequired(server))
    }

    if (ns.getServerNumPortsRequired(server) > open_ports) {
      ns.tprint("  Not enough open ports available to hack " + server)
      continue
    }

    //if (ns.getServerRequiredHackingLevel(server) > ns.getHackingLevel()) {
    //  ns.tprint("  Current hacking level too low for " + server)
    //  continue
    //}

    try {
      ns.nuke(server)
    } catch {
      ns.tprint("  Failed to nuke ${server}")
      continue
    }

    if (ns.hasRootAccess(server)) {
      ns.tprint("  *** Root access now available for " + server + " ***")
      cracked_servers.push(server)
    }
  }

  ns.tprint("Cracked servers: " + cracked_servers.join(" "))
}
