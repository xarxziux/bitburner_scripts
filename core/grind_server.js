/** @param {NS} ns */
export async function main(ns) {
  const threads = ns.args[0]
  const target = ns.args[1]

  if (threads == undefined) {
    ns.tprint("Usage: ./grind_n00dles.js <threads>")
    return
  }

  if (!ns.serverExists(target)) {
    ns.tprint("Server " + target + " not found")
    return
  }

  ns.tprint("Grinding " + target + " with " + threads + " threads")

  while(true) {
    await ns.hack(target, {threads: threads})
  }
}
