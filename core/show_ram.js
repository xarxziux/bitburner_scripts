//import { SERVER_PORT } from "./constants.js"
import { get_core_servers, sort_by_largest_value } from "shared/utils.js"

/** @param {NS} ns */
export async function main(ns) {
  //const servers = ns.getPortHandle(SERVER_PORT).data
  const servers = get_core_servers(ns)
  const min = ns.args[0] || 0
  const max = ns.args[1] || Infinity
  const items = []

  servers.forEach((server) => {
    if (!ns.hasRootAccess(server)) {
      return
    }

    const total_ram = ns.getServerMaxRam(server)
    const in_use = (ns.getServerUsedRam(server) !== 0)

    //ns.tprint("Server: " + server + ", total RAM: " + total_ram + ",in use: " + in_use)

    if (total_ram >= min && total_ram <= max) {
      items.push({
        server: server,
        value: total_ram,
        in_use: in_use
      })
    }
  })

  items.sort(sort_by_largest_value)

  items.forEach(item => {
    //ns.tprint("Server: " + item.server + ", total RAM: " + item.value + ",in use: " + item.in_use)

    if (item.in_use) {
      ns.tprint(item.server + ": " + item.value + " *")
    } else {
      ns.tprint(item.server + ": " + item.value)
    }
  })
}