/** @param {NS} ns */
export async function main(ns) {
  const home = "home"
  const host = ns.getHostname()

  if (host == home) {
    ns.tprint("This script needs to be run on your new home server")
    return
  }

  ns.scp(ns.ls(home, "shared/"), host, home)
  ns.scp(ns.ls(home, "core/"), host, home)
  ns.scp(ns.ls(home, "host_files/"), host, home)
  ns.scp("startup/grind_n00dles.js", host, home)
}