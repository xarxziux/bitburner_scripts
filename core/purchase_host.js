/** @param {NS} ns */
export async function main(ns) {
  const server_ram = ns.args[0]

  for (let i = 1; i < ns.args.length; i++) {
    await ns.sleep(1000)

    const target_server = ns.args[i]
    ns.tprint("Processing " + target_server)

    if (!ns.serverExists(target_server)) {
      ns.tprint("  Invalid server name " + target_server)
      continue
    }

    const host_server = target_server + "-hack"

    if (ns.serverExists(host_server)) {
      ns.tprint("  Purchased server " + host_server + " already exists")
      continue
    }

    ns.tprint("  New server cost: " + ns.getPurchasedServerCost(server_ram))
    ns.tprint("  Total funds - new server cost = " +
      (ns.getServerMoneyAvailable("home") - ns.getPurchasedServerCost(server_ram))
    )

    if (ns.getPurchasedServerCost(server_ram) > ns.getServerMoneyAvailable("home")){
      ns.tprint("  Insufficient funds available")
      break
    }

    const host = ns.purchaseServer(host_server, server_ram)
    ns.tprint("  Host: " + host)
    ns.run("core/start_host.js", 1, host_server + "_" + target_server)
  }
}
