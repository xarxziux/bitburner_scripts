/** @param {NS} ns */
export async function main(ns) {
  if (ns.getHostname() != "home") {
    ns.tprint("Please run this script on the home server")
    return
  }

  const max_ram = ns.getServerMaxRam("home") - ns.getServerUsedRam("home") - 20
  const threads = Math.floor(max_ram / 1.7)

  if (threads <= 0) {
    ns.tprint("Insufficient RAM available on home to run grind_n00dles.js")
    return
  }

  ns.run("core/grind_n00dles.js", threads, threads)
}
