/** @param {NS} ns */
export async function main(ns) {
  const host_files = ns.ls("home", "host_files/")
  const shared_files = ns.ls("home", "shared/")

  for (let i = 0; i < ns.args.length; i++) {
    await ns.sleep(1000)

    const server = ns.args[i]
    ns.tprint("Processing " + server)

    if (!ns.serverExists(server)) {
      ns.tprint("  Unrecognised server " + server)
      continue
    }

    ns.write("target.txt", server, "w")

    const ok = ns.scp("target.txt", server)
    if (!ok) {
      ns.tprint("  Failed to upload target file")
      continue
    }

    ns.rm("target.txt")

    ns.tprint("  Starting scripts on " + server)
    ns.scp(host_files, server)
    ns.scp(shared_files, server)
    const pid1 = ns.exec(
      "host_files/weaken_controller.js", server, 1, server)

    if (pid1 == 0) {
      ns.tprint("  Failed to start weaken contoller on " + server)
    } else {
      ns.tprint("  Started weaken controller on " + server)
    }

    const pid2 = ns.exec(
      "host_files/grow_controller.js", server, 1, server)

    if (pid2 == 0) {
      ns.tprint("  Failed to start grow controller on " + server)
    } else {
      ns.tprint("  Started grow controller on " + server)
    }

    const pid3 = ns.exec(
      "host_files/hack_controller.js", server, 1, server)

    if (pid3 == 0) {
      ns.tprint("  Failed to start hack controller on " + server)
    } else {
      ns.tprint("  Started hack controller on " + server)
    }
  }

  ns.tprint("start_self_host script completed")
}
