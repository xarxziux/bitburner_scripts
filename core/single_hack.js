/** @param {NS} ns */
export async function main(ns) {
  const target_server = ns.args[0]
  const threads = ns.args[1]

  if (threads == undefined) {
    ns.tprint("Usage: single_hack.js -t <threads> <target_server> <threads>")
    return
  }

  ns.tprint("Hack time = " + ns.getHackTime(target_server))
  const loot = await ns.hack(target_server, {threads: threads})
  ns.tprint("Hack completed.  Gained $" + loot)
}