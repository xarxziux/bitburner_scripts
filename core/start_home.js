/** @param {NS} ns */
export async function main(ns) {
  const host_server = "home"

  for (let i = 0; i < ns.args.length; i++) {
    await ns.sleep(1000)

    const target_server = ns.args[i]
    ns.tprint("Processing " + target_server)

    if (!ns.serverExists(target_server)) {
      ns.tprint("  Unrecognised target server " + target_server)
      continue
    }

    ns.write("target.txt", host_server, "w")

    const ok = ns.scp("target.txt", target_server)
    if (!ok) {
      ns.tprint("  Failed to upload target file")
      continue
    }

    ns.rm("target.txt")

    ns.tprint("  Starting scripts on " + host_server)
    const pid1 = ns.exec("host_files/weaken_controller.js", host_server, 1, target_server)

    if (pid1 == 0) {
      ns.tprint("  Failed to start weaken contoller on " + host_server)
    } else {
      ns.tprint("  Started weaken controller on " + host_server)
    }

    const pid2 = ns.exec("host_files/grow_controller.js", host_server, 1, target_server)

    if (pid2 == 0) {
      ns.tprint("  Failed to start grow controller on " + host_server)
    } else {
      ns.tprint("  Started grow controller on " + host_server)
    }

    const pid3 = ns.exec("host_files/hack_controller.js", host_server, 1, target_server)

    if (pid3 == 0) {
      ns.tprint("  Failed to start hack controller on " + host_server)
    } else {
      ns.tprint("  Started hack controller on " + host_server)
    }
  }

  ns.tprint("start_host_script completed")
}
