/** @param {NS} ns */
export async function main(ns) {
  const servers = ns.getPurchasedServers()
  const target_ram = 0 + ns.args[0]

  for (const server of servers) {
    await ns.sleep(1000)

    ns.tprint("Processing " + server)

    if(ns.getServerMaxRam(server) >= target_ram) {
      ns.tprint("  No upgrade required for " + server)
      continue
    }

    const upgrade_cost = ns.getPurchasedServerUpgradeCost(
      server, target_ram)

    if (upgrade_cost == 0) {
      ns.tprint("  Server " + server + " already at " + target_ram + "Gb")
      continue
    }

    const current_funds = ns.getServerMoneyAvailable("home")

    if (upgrade_cost > current_funds) {
      ns.tprint("  Not enough funds available to upgrade " + server)
      break
    }

    const ok = ns.upgradePurchasedServer(server, target_ram)

    if (!ok) {
      ns.tprint("  Failed to upgrade " + server)
      break
    }

    ns.tprint("  Server " + server + " upgraded to " + target_ram + "Gb")
  }

  ns.tprint("Script complete")
}
