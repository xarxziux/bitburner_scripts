import { get_core_servers } from "utils.js"

/** @param {NS} ns */
export async function main(ns) {
  const servers = get_core_servers()
  const scripts = [
    "weaken_controller.js", "grow_controller.js", "hack_controller.js"]

  for (const server of servers) {
    await ns.sleep(500)
    for (const script of scripts) {
      await ns.sleep(500)
      ns.tprint("Looking for " + script + " on " + server)
      const running = ns.getRunningScript(script, server, server)
      if (running) {
        ns.tprint("  Killing " + script + " on " + server)
        ns.kill(running.pid)
      }
    }
  }
}

