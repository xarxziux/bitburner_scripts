/** @param {NS} ns */
export async function main(ns) {
  const server = "w0r1d_d43m0n"

  if (!ns.serverExists(server) ||
    !ns.fileExists("BruteSSH.exe", "home") ||
    !ns.fileExists("FTPCrack.exe", "home") ||
    !ns.fileExists("relaySMTP.exe", "home") ||
    !ns.fileExists("HTTPWorm.exe", "home") ||
    !ns.fileExists("SQLInject.exe", "home")) {
    ns.tprint("You are not ready")
  }

  ns.brutessh(server)
  ns.ftpcrack(server)
  ns.relaysmtp(server)
  ns.httpworm(server)
  ns.sqlinject(server)
  ns.nuke(server)

  ns.tprint("You are ready")
}