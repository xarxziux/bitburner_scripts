import { get_core_servers } from "shared/utils.js"

/** @param {NS} ns */
export async function main(ns) {
  const host_files = ns.ls("home", "host_files/")
  const shared_files = ns.ls("home", "shared/")
  const core_servers = get_core_servers()
  const purchased_server = ns.getPurchasedServers()
  const servers = core_servers.concat(purchased_server)

  for (const server of servers) {
    await ns.sleep(1000)

    ns.tprint("Copying files to " + server)
    ns.scp(host_files, server)
    ns.scp(shared_files, server)
  }

  ns.tprint("Files copied")
}
