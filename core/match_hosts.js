import { get_core_servers, sort_by_smallest_value } from "shared/utils.js"

/** @param {NS} ns */
export async function main(ns) {
  const servers = get_core_servers()
  const hosts = []
  const targets = []

  for (const server of servers) {
    await ns.sleep(200)
    ns.tprint("Processing " + server)

    if ((ns.serverExists(server)) &&
      (ns.hasRootAccess(server)) &&
      (ns.getServerUsedRam(server) == 0) &&
      (ns.getServerMaxRam(server) >= 32)) {
      hosts.push({ server: server, value: ns.getServerMaxRam(server) })
    }

    if ((ns.serverExists(server)) &&
      (server != "n00dles") &&
      (ns.getServerMaxMoney(server) > 0) &&
      (ns.hasRootAccess(server)) &&
      //(ns.getServerRequiredHackingLevel(server) <= ns.getHackingLevel()) &&
      (!ns.fileExists("target.txt", server))) {
      targets.push({ server: server, value: ns.getServerMaxMoney(server) })
    }
  }

  hosts.sort(sort_by_smallest_value)
  targets.sort(sort_by_smallest_value)
  const matches = Math.min(hosts.length, targets.length)

  for (let i = 0; i < matches; i++) {
    ns.run("core/start_host.js", 1, hosts[i].server + "_" + targets[i].server)
    await ns.sleep(1000)
  }

  ns.tprint("match_hosts script complete")
}
