/** @param {NS} ns */
export async function main(ns) {
  const testPort = ns.getPortHandle(1)
  let i = 0
  for (i; i < 1000; i++) {
    const ok = ns.tryWritePort(1, i)
    if (!ok) {
      ns.tprint("Failed to write to port at i = " + i)
      break
    }
  }

  ns.tprint("Sent " + i + " data points to port")
}