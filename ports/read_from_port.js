/** @param {NS} ns */
export async function main(ns) {
  while(true) {
    const data = ns.readPort(1)

    if (data == "NULL PORT DATA") {
      await ns.sleep(1000)
      continue
    }
    
    ns.tprint("Port 1 contains: " + data)
  }
}