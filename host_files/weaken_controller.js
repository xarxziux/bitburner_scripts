import { LOCK_QUEUE_SET } from "shared/constants.js"

/** @param {NS} ns */
export async function main(ns) {
  await ns.sleep(10000)

  const host_server = ns.getHostname()
  const target_server = ns.args[0]
  const lock_file = host_server + ".weaken." + target_server + ".lock.txt"

  if (!ns.serverExists(target_server)) {
    ns.tprint(
      "Invalid server param.  Usage: weaken_controller <target_server>")
    return
  }

  while (true) {
    await ns.sleep(1000)

    if (ns.fileExists(lock_file, "home")) {
      ns.print("Lock file " + lock_file + " already set")
      continue
    }

    const needed_threads = Math.floor((
      ns.getServerSecurityLevel(target_server) -
      ns.getServerMinSecurityLevel(target_server)) * 20)

    if (needed_threads <= 0) {
      ns.print("Server security already weakened")
      continue
    }

    const available_threads = Math.floor((
      ns.getServerMaxRam(host_server) -
      ns.getServerUsedRam(host_server)) / 1.75)

    if (available_threads <= 0) {
      ns.print("No free RAM. Sleeping")
      continue
    }

    const actual_threads = Math.min(available_threads, needed_threads)

    if (actual_threads <= 0) {
      ns.print("Actual threads = 0")
      continue
    }

    const pid = ns.run("host_files/lock_weaken.js", actual_threads, host_server,
      target_server, actual_threads)

    if (pid != 0) {
        ns.tryWritePort(LOCK_QUEUE_SET, lock_file + " pid:" + pid)
    }

    await ns.sleep(ns.getWeakenTime(target_server) + 1000)
  }
}