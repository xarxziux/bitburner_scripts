import { MESSAGE_QUEUE, LOCK_QUEUE_UNSET } from "shared/constants.js"

/** @param {NS} ns */
export async function main(ns) {
  const host_server = ns.args[0]
  const target_server = ns.args[1]
  const threads = ns.args[2]
  const lock_file = host_server + ".weaken." + target_server + ".lock.txt"

  ns.atExit(unset_lock_file(ns, lock_file))

  const w = await ns.weaken(target_server, { stock: true, threads: threads })
  ns.tryWritePort(MESSAGE_QUEUE, "From " + host_server +
    ": security on " + target_server + " lowered by " + w)
}

const unset_lock_file = (ns, lock_file) => () => {
  ns.tryWritePort(LOCK_QUEUE_UNSET, lock_file)
}