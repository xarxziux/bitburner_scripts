import { LOCK_QUEUE_SET, LOCK_QUEUE_UNSET } from "shared/constants.js"

/** @param {NS} ns */
export async function main(ns) {
  await ns.sleep(10000)

  const host_server = ns.getHostname()
  const target_server = ns.args[0]

  if (!ns.serverExists(target_server)) {
    ns.tprint("Invalid server param to hack_controller script.  " +
      "Usage: hack_controller <target_server>")
    return
  }

  const min_security = ns.getServerMinSecurityLevel(target_server)
  const max_ram = ns.getServerMaxRam(host_server)
  const max_funds = ns.getServerMaxMoney(target_server)
  const lock_file = host_server + ".hack." + target_server + ".lock.txt"

  while (true) {
    await ns.sleep(1000)

    if (!ns.hasRootAccess(target_server)) {
      ns.print("No root access to " + target_server)
      continue
    }

    if (ns.getHackingLevel() < ns.getServerRequiredHackingLevel(target_server)) {
      ns.print("Hacking level too low to hack " + target_server)
      await ns.sleep(600000)
      continue
    }

    if (ns.fileExists(lock_file, "home")) {
      ns.print("Lock file " + lock_file + " already in place.  Sleeping")
      continue
    }

    const curr_security = ns.getServerSecurityLevel(target_server)

    if (curr_security >= min_security * 1.2) {
      ns.print("Security level too high")
      continue
    }

    const curr_funds = ns.getServerMoneyAvailable(target_server)

    if (curr_funds <= max_funds * 0.95) {
      ns.print("Not enough funds to run hack")
      continue
    }

    const target_funds = max_funds * 0.15
    const hackable_funds = (curr_funds <= target_funds
      ? 0
      : curr_funds - target_funds)


    if (hackable_funds == 0) {
      ns.print("Server funds already drained:  target = " +
        target_funds + ", current = " + curr_funds)
      continue
    }

    const required_threads = Math.floor(
      ns.hackAnalyzeThreads(target_server, hackable_funds))

    if (required_threads <= 0) {
      ns.print("hackAnalyzeThreads() call failed")
      continue
    }

    const available_threads = Math.floor(
      (max_ram - ns.getServerUsedRam(host_server)) / 1.70)

    if (available_threads <= 0) {
      ns.print("No free RAM. Sleeping")
      continue
    }

    const actual_threads = Math.min(available_threads, required_threads)
    
    const pid = ns.run("host_files/lock_hack.js", actual_threads, host_server,
      target_server, actual_threads)

    if (pid != 0) {
      ns.tryWritePort(LOCK_QUEUE_SET, lock_file + " pid:" + pid)
    }

    await ns.sleep(ns.getHackTime(target_server))
  }
}