import { LOCK_QUEUE_SET, LOCK_QUEUE_UNSET } from "shared/constants.js"

/** @param {NS} ns */
export async function main(ns) {
  await ns.sleep(10000)

  const host_server = ns.getHostname()
  const target_server = ns.args[0]

  if (!ns.serverExists(target_server)) {
    ns.tprint("Invalid server param to grow_controller script.  " +
      "Usage: grow_controller <target_server>")
    return
  }

  const min_security = ns.getServerMinSecurityLevel(target_server)
  const max_funds = ns.getServerMaxMoney(target_server)
  const lock_file = host_server + ".grow." + target_server + ".lock.txt"

  while (true) {
    await ns.sleep(1000)

    const max_ram = ns.getServerMaxRam(host_server)

    if (ns.fileExists(lock_file, "home")) {
      ns.print("Lock file " + lock_file + " already in place.  Sleeping")
      continue
    }

    const curr_security = ns.getServerSecurityLevel(target_server)

    if (curr_security >= (min_security * 3 / 2)) {
      ns.print("Security level too high")
      continue
    }

    const curr_funds = ns.getServerMoneyAvailable(target_server)
    const min_funds = max_funds / 10
    const grow_mult = ((curr_funds < min_funds)
      ? 10
      : Math.max(max_funds / curr_funds, 1))
    
    if (Number.isNaN(grow_mult)) {
      ns.print("grow_mult is NaN")
      await ns.sleep(60000)
      continue
    }

    const required_threads = Math.floor(
      ns.growthAnalyze(target_server, grow_mult))

    if (required_threads <= 0) {
      ns.print("Server funds already maxed out:  max = " + max_funds +
        ", current = " + curr_funds + ", mult = " + grow_mult)
      continue
    }

    const available_threads = Math.floor(
      (max_ram - ns.getServerUsedRam(host_server)) / 1.75)

    if (available_threads <= 0) {
      ns.print("No free RAM. Sleeping")
      continue
    }

    const actual_threads = Math.min(available_threads, required_threads)
    
    const pid = ns.run("host_files/lock_grow.js", actual_threads, host_server,
      target_server, actual_threads)

    if (pid != 0) {
      ns.tryWritePort(LOCK_QUEUE_SET, lock_file + " pid:" + pid)
    }

    await ns.sleep(ns.getGrowTime(target_server))
  }
}