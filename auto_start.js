/** @param {NS} ns */
export async function main(ns) {
  const home = "home"
  ns.nuke("n00dles")

  const startup_scripts = [
    "lock_file_cleanup.js",
    "lock_queue_set_monitor.js",
    "lock_queue_unset_monitor.js",
    "master_log.js",
  ]

  for (const script of startup_scripts) {
    ns.tprint("Starting " + script)
    ns.run("./startup/" + script)
  }

  ns.tprint("Starting manage_fighters.js")
  ns.run("gangs/manage_fighters.js")

  await await_pid(ns, ns.run("core/start_home_grind.js"))
  await await_pid(ns, ns.run("core/hack_all.js"))
  await await_pid(ns, ns.run("core/start_grind.js"))
  await await_pid(ns, ns.run("core/match_hosts.js"))
}

/** 
 * @param {NS} ns
 * @param (number) pid 
 */
async function await_pid(ns, pid) {
  while (ns.isRunning(pid)) {
    await ns.sleep(60000)
  }
}
